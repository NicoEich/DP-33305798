package ha04;


import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Writing
{

	public static void storeStringInFile(String string, String path){
		try (PrintWriter out = new PrintWriter(path)) {
			out.println(string);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
}
