package ha04;

public class SaveTask implements Runnable
{
	String path;

	public SaveTask(String path)
	{
		this.path = path;
	}

	public void run()
	{
		Timer.storeCurrentDateInFile(path);
	}
}
