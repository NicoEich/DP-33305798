package ha04;


import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;



public class Timer
{

	public static void startTimer(){
		SaveTask save = new SaveTask("time.txt");        

		ScheduledExecutorService execService = Executors.newScheduledThreadPool(1);
		execService.scheduleAtFixedRate(save, 0, 2000, TimeUnit.MILLISECONDS);  // run save every 2 seconds
		
	}

	public static void storeCurrentDateInFile(String filePath)
	{
		String currentTime = Dateformat.getCurrentTime();
		Writing.storeStringInFile(currentTime, filePath);
		System.out.println("Time: " + currentTime + " saved in " + filePath);
	}
}
