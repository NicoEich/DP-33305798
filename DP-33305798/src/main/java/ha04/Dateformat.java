package ha04;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Dateformat
{
	public static String getCurrentTime()
	{
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date(); 
		
		String time = dateFormat.format(date);
		return time;
	}
}
