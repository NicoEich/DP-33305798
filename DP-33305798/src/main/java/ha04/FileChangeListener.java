package ha04;

import java.io.IOException;
import java.nio.file.*;

public class FileChangeListener {

	public static void startListen() throws IOException, InterruptedException {

		WatchService watchService = FileSystems.getDefault().newWatchService();

		Path path = Paths.get("."); // save in this Dir.

		path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY); // just listen to modify 
		
		WatchKey watchKey;  // events

		while ((watchKey = watchService.take()) != null) {
			for (WatchEvent<?> event : watchKey.pollEvents()) {
				
				System.out.println( "saved Time is: " + Reading.getText("time.txt"));
			}
			watchKey.reset();
		}

	}
}