package ha03;

import java.util.Stack;

public class PrintHandler extends Handler {

	public void interprete(String input, Stack<Integer> s) {

		if (input.contains("Print")) {

			System.out.println(s.pop());

		}
	}

}
