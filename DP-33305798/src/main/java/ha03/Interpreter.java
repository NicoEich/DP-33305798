package ha03;

import java.util.ArrayList;
import java.util.Stack;

public class Interpreter {

	
	private Stack<Integer> s = new Stack();
	private ArrayList<Handler> handleStrategies = new ArrayList<Handler>();
	
	public void interpret(String input) {
		
		for(Handler h : handleStrategies) {
			
			h.interprete(input, s);
			
		}
	}
	
	public void interpretText(String[] parts) {
		
		for(String s : parts) {
			interpret(s);
		}
	}
	
	public void addHandler(Handler h) {
		
		handleStrategies.add(h);
	}
	
	public void addDefaultHandler() {
		
		
		addHandler(new LdcHandler());
		addHandler(new MultHandler());
		addHandler(new StoreHandler());
		addHandler(new LdHandler());
		addHandler(new PrintHandler());
		
	}
	
	
	
	public void writeOnStack(int n) {
		
		s.add(n);
	}
	
	public int readFromStack() {
		
		return s.pop();
	}
	
	public Stack getStack() {
		
		return s;
	}
}
