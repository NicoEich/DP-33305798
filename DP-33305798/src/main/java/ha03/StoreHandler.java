package ha03;

import java.util.Stack;

public class StoreHandler extends Handler {

	public void interprete(String input, Stack<Integer> s) {

		if (input.contains("Store")) {

			
			char variable = input.charAt(6); 
			
			int value = s.pop();
			
			
			saveValue(value,variable);

		}
	}

	private void saveValue(int value, char variable) { // save value in Variable 
		
		System.out.println("Der Wert "+ value + " wurde gespeichert in " + variable);
		
	}

}
