package ha06;

import java.io.IOException;

import org.junit.Test;
import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.debug.FlipBook;
import org.sdmlib.serialization.SDMLibJsonIdMap;

import de.uniks.networkparser.graph.Cardinality;
import de.uniks.networkparser.graph.Clazz;
import de.uniks.networkparser.graph.DataType;
import de.uniks.networkparser.graph.Parameter;
import ha06.util.NodeCreator;

public class CodeGenerator {


	@Test
	public void test() {
		
		
		ClassModel model = new ClassModel("ha06");
		
		
		SDMLibJsonIdMap idMap = (SDMLibJsonIdMap) NodeCreator.createIdMap("ajz"); // cant cast IdMap to SDMLIJASONIDMAP 
		
		FlipBook flipbook = idMap.createFlipBook();
		
		Clazz node = model.createClazz("Node").withAttribute("name", DataType.STRING);     
		
		Clazz line = model.createClazz("MyLine").withSuperClazz(node).withAttribute("name", DataType.STRING).withAttribute("startX", DataType.INT).withAttribute("startY", DataType.INT).withAttribute("endX", DataType.INT).withAttribute("endY", DataType.INT); // TODO: add arraylist children
		
		Clazz group = model.createClazz("Group").withSuperClazz(node).withAttribute("name", DataType.STRING);
			
		group.createBidirectional(line, "linie", Cardinality.MANY, "gruppe", Cardinality.ONE);  // should every line and groupe just have 1 groupe ? ifnot change cardinality
		
		group.createBidirectional(group, "gruppen", Cardinality.MANY, "gruppen", Cardinality.MANY); 
		
		
		Clazz handler = model.createClazz("Handler").withMethod("interprete", DataType.VOID, new Parameter(DataType.STRING).with("input"));
		
		Clazz drawLine = model.createClazz("DrawLine").withSuperClazz(handler).withMethod("interprete", DataType.VOID, new Parameter(DataType.STRING).with("input"));
		
		Clazz groupUp = model.createClazz("GroupUp").withSuperClazz(handler).withMethod("interprete", DataType.VOID, new Parameter(DataType.STRING).with("input"));
		
		Clazz clone = model.createClazz("Clone").withSuperClazz(handler).withMethod("interprete", DataType.VOID, new Parameter(DataType.STRING).with("input"));
		
		Clazz undo = model.createClazz("Undo").withSuperClazz(handler).withMethod("interprete", DataType.VOID, new Parameter(DataType.STRING).with("input"));
	
		model.generate();
	}
}
