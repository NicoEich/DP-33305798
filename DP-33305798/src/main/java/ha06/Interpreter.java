package ha06;

import java.util.ArrayList;


public class Interpreter {

	public Interpreter() {
		addDefaultHandler();
	}

	private ArrayList<Handler> handleStrategies = new ArrayList<Handler>();
	
	public void interpret(String input, Main main) {
		
		for(Handler h : handleStrategies) {
			
			h.interprete(input, main);
			
		}
	}
	
	
	public void addHandler(Handler h) {
		
		handleStrategies.add(h);
	}
	
	public void addDefaultHandler() {
		
		
		addHandler(new DrawLine());
		addHandler(new GroupUp());
		addHandler(new Clone());
		addHandler(new Undo());
		
	}
	

}
