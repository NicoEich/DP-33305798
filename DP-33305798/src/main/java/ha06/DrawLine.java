/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package ha06;

import ha06.Handler;

public  class DrawLine extends Handler
{

   
   //==========================================================================
	@Override
   public void interprete( String input ,Main main)
   {
	   if(input.contains("line")) {
			
		   String[] parts = input.split(" ");
		   String name = parts[1];
		   
		   for (MyLine l : main.getGroup().getLinie() ) {
			  if( l.getName().equals(name) ) {
				  System.out.println("Fehler die Linie gibt es schon");
			  }
		   }
		   int startX = Integer.parseInt(parts[2]);
		   int startY = Integer.parseInt(parts[3]);		   
		   int endX = Integer.parseInt(parts[4]);
		   int endY = Integer.parseInt(parts[5]);
		   
		  MyLine myLine = main.getGroup().createLinie().withName(name).withStartX(startX).withStartY(startY).withEndX(endX).withEndY(endY);
		  main.drawLine(myLine);
		}
   }

   
   //==========================================================================
   
   @Override
   public void removeYou()
   {
      firePropertyChange("REMOVE_YOU", this, null);
   }

   
   //==========================================================================
   public void interprete( String input )
   {
      
   }
}
