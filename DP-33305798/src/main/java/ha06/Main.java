package ha06;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

	private Pane pane;
	private Group all;   // group with all other Groups and Lines instead of a node
	private Interpreter interpreter;
	private TextField textfield ;
	private Button insertButton ;
	
	
	
	@Override
	public void start(Stage stage) {
		interpreter = new Interpreter();
		all = new Group();
		
		VBox root = new VBox();
		root.setSpacing(10);
		Scene scene = new Scene(root, 1000, 500);
		
		pane = new Pane();
		pane.setMinSize(900, 400);
		
		 textfield = new TextField();
		
		 insertButton = new Button();
	        insertButton.setText("Enter");
	        insertButton.setPrefWidth(220);
		
	        insertButton.setOnAction((e) ->{
	           
	        	String text = textfield.getText();
	        	System.out.println(text);
	        	textfield.setText("");
	        	
	        	interpreter.interpret(text,this);  // 

	        });    
	        
	        
	        
	        
		root.getChildren().addAll(pane,textfield,insertButton);
		stage.setTitle("Draw your Lines");
		stage.setScene(scene);
		
		stage.show();
	}


	

	public void drawLine(MyLine myLine) {
		
		Line line = new Line(myLine.getStartX(),myLine.getStartY(),myLine.getEndX(),myLine.getEndY());
		getPane().getChildren().add(line);
		
	}
	
	public void interprete(String text) {
		
		interpreter.interpret(text,this);
		
	}
	
	public Group getGroup() {
		return all;
	}
	public Pane getPane() {
		return pane;
	}
	public Interpreter getInterpreter() {
		return interpreter;
	}
	public Button getButton() {
		return insertButton;
	}
	public TextField getTextfield() {
		return textfield;
	}
	
}
