/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package ha06;

import ha06.Handler;

public class GroupUp extends Handler {

	// ==========================================================================
	@Override
	public void interprete(String input, Main main) {
		if (input.contains("group")) {

			String[] parts = input.split(" ");
			String name = parts[1];

			for (Group g : main.getGroup().getGruppen()) { // check if this Group allready exists
				if (g.getName() == name) {
					System.out.println("Fehler die Gruppe gibt es schon");
				}
			}

			Group group = main.getGroup().createGruppen().withName(name);		


			for (int i= 2 ; i < parts.length ;i++) {		// for every string part
				
				String s = parts[i];
				
				if (s.contains("g")) {

					for (Group g : main.getGroup().getGruppen()) { // if Group exists add to this group
						if (g.getName().equals(s)) {
							group.withGruppen(g); // add old group g to new group group
						}
					}

				}
		
				if(s.contains("l")) {
					for( int u = 0; u < main.getGroup().getLinie().size() ; u ++) {
						
						 MyLine l = main.getGroup().getLinie().get(u);
						if (l.getName().equals(s) ) {
							group.withLinie(l);
							System.out.println(l.getName()+ "add to group");
						}
					}
				}
				
			}

		}
	}

	// ==========================================================================

	@Override
	public void removeYou() {
		firePropertyChange("REMOVE_YOU", this, null);
	}

	// ==========================================================================
	public void interprete(String input) {

	}
}
