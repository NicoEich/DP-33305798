/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package ha06;

import ha06.Handler;

public class Clone extends Handler {

	// ==========================================================================
	@Override
	public void interprete(String input, Main main) {
		
		if (input.contains("clone")) {
			String[] parts = input.split(" ");

			String origninalGroup = parts[1];
			String newGroup = parts[2];
			int offsetX = Integer.parseInt(parts[3]);
			int offsetY = Integer.parseInt(parts[4]);

			for (Group g : main.getGroup().getGruppen()) { // check if this Group allready exists
				if (g.getName().equals(newGroup)) {
					System.out.println("Fehler die Gruppe gibt es schon");
				}
			}
			
			Group group = main.getGroup().createGruppen().withName(newGroup);

			for (Group g : main.getGroup().getGruppen()) {

				if (g.getName().contains(origninalGroup)) {			
					
					for( MyLine l : g.getLinie()) {
						
						MyLine newLine  = group.createLinie().withName(l.getName()+"a").withStartX(l.getStartX()+offsetX).withStartY(l.getStartY()+offsetY)
								.withEndX(l.getEndX()+offsetX).withEndY(l.getEndY()+offsetY);					// create new Line with old Line + offset
						
						main.drawLine(newLine);
					}
					
				}
			}
		}
		
	}

	// ==========================================================================

	@Override
	public void removeYou() {
		firePropertyChange("REMOVE_YOU", this, null);
	}

	// ==========================================================================
	public void interprete(String input) {

	}
}
