/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package ha06;

import ha06.Node;
import ha06.Group;
import de.uniks.networkparser.EntityUtil;

public  class MyLine extends Node
{


  


   @Override
   public String toString()
   {
      StringBuilder result = new StringBuilder();
      
      result.append(" ").append(this.getName());
      result.append(" ").append(this.getEndX());
      result.append(" ").append(this.getEndY());
      result.append(" ").append(this.getStartX());
      result.append(" ").append(this.getStartY());
      return result.substring(1);
   }


   
   /********************************************************************
    * <pre>
    *              many                       one
    * Line ----------------------------------- Group
    *              linie                   gruppe
    * </pre>
    */
   
   public static final String PROPERTY_GRUPPE = "gruppe";

   private Group gruppe = null;

   public Group getGruppe()
   {
      return this.gruppe;
   }

   public boolean setGruppe(Group value)
   {
      boolean changed = false;
      
      if (this.gruppe != value)
      {
         Group oldValue = this.gruppe;
         
         if (this.gruppe != null)
         {
            this.gruppe = null;
            oldValue.withoutLinie(this);
         }
         
         this.gruppe = value;
         
         if (value != null)
         {
            value.withLinie(this);
         }
         
         firePropertyChange(PROPERTY_GRUPPE, oldValue, value);
         changed = true;
      }
      
      return changed;
   }

   public MyLine withGruppe(Group value)
   {
      setGruppe(value);
      return this;
   } 

   public Group createGruppe()
   {
      Group value = new Group();
      withGruppe(value);
      return value;
   } 

   
   //==========================================================================
   
   public static final String PROPERTY_ENDX = "endX";
   
   private int endX;

   public int getEndX()
   {
      return this.endX;
   }
   
   public void setEndX(int value)
   {
      if (this.endX != value) {
      
         int oldValue = this.endX;
         this.endX = value;
         this.firePropertyChange(PROPERTY_ENDX, oldValue, value);
      }
   }
   
   public MyLine withEndX(int value)
   {
      setEndX(value);
      return this;
   } 

   
   //==========================================================================
   
   public static final String PROPERTY_ENDY = "endY";
   
   private int endY;

   public int getEndY()
   {
      return this.endY;
   }
   
   public void setEndY(int value)
   {
      if (this.endY != value) {
      
         int oldValue = this.endY;
         this.endY = value;
         this.firePropertyChange(PROPERTY_ENDY, oldValue, value);
      }
   }
   
   public MyLine withEndY(int value)
   {
      setEndY(value);
      return this;
   } 

   
   //==========================================================================
   
   public static final String PROPERTY_STARTX = "startX";
   
   private int startX;

   public int getStartX()
   {
      return this.startX;
   }
   
   public void setStartX(int value)
   {
      if (this.startX != value) {
      
         int oldValue = this.startX;
         this.startX = value;
         this.firePropertyChange(PROPERTY_STARTX, oldValue, value);
      }
   }
   
   public MyLine withStartX(int value)
   {
      setStartX(value);
      return this;
   } 

   
   //==========================================================================
   
   public static final String PROPERTY_STARTY = "startY";
   
   private int startY;

   public int getStartY()
   {
      return this.startY;
   }
   
   public void setStartY(int value)
   {
      if (this.startY != value) {
      
         int oldValue = this.startY;
         this.startY = value;
         this.firePropertyChange(PROPERTY_STARTY, oldValue, value);
      }
   }
   
   public MyLine withStartY(int value)
   {
      setStartY(value);
      return this;
   } 

   
   //==========================================================================
   
   public static final String PROPERTY_NAME = "name";
   
   private String name;

   public String getName()
   {
      return this.name;
   }
   
   public void setName(String value)
   {
      if ( ! EntityUtil.stringEquals(this.name, value)) {
      
         String oldValue = this.name;
         this.name = value;
         this.firePropertyChange(PROPERTY_NAME, oldValue, value);
      }
   }
   
   public MyLine withName(String value)
   {
      setName(value);
      return this;
   } 

   
   //==========================================================================
   
   @Override
   public void removeYou()
   {
      setGruppe(null);
      firePropertyChange("REMOVE_YOU", this, null);
   }
}
