package ha06.util;

import org.sdmlib.models.pattern.PatternObject;
import ha06.Clone;

public class ClonePO extends PatternObject<ClonePO, Clone>
{

    public CloneSet allMatches()
   {
      this.setDoAllMatches(true);
      
      CloneSet matches = new CloneSet();

      while (this.getPattern().getHasMatch())
      {
         matches.add((Clone) this.getCurrentMatch());
         
         this.getPattern().findMatch();
      }
      
      return matches;
   }


   public ClonePO(){
      newInstance(null);
   }

   public ClonePO(Clone... hostGraphObject) {
      if(hostGraphObject==null || hostGraphObject.length<1){
         return ;
      }
      newInstance(null, hostGraphObject);
   }

   public ClonePO(String modifier)
   {
      this.setModifier(modifier);
   }
   
   //==========================================================================
   
   public void interprete(String input)
   {
      if (this.getPattern().getHasMatch())
      {
          ((Clone) getCurrentMatch()).interprete(input);
      }
   }

}
