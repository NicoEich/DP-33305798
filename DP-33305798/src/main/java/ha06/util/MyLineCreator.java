/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package ha06.util;

import de.uniks.networkparser.interfaces.AggregatedEntityCreator;
import ha06.MyLine;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.networkparser.IdMap;
import ha06.Group;

public class MyLineCreator implements AggregatedEntityCreator
{
   public static final MyLineCreator it = new MyLineCreator();
   
   private final String[] properties = new String[]
   {
      MyLine.PROPERTY_ENDX,
      MyLine.PROPERTY_ENDY,
      MyLine.PROPERTY_NAME,
      MyLine.PROPERTY_STARTX,
      MyLine.PROPERTY_STARTY,
      MyLine.PROPERTY_GRUPPE,
   };
   
   private final String[] upProperties = new String[]
   {
   };
   
   private final String[] downProperties = new String[]
   {
   };
   
   @Override
   public String[] getProperties()
   {
      return properties;
   }
   
   @Override
   public String[] getUpProperties()
   {
      return upProperties;
   }
   
   @Override
   public String[] getDownProperties()
   {
      return downProperties;
   }
   
   @Override
   public Object getSendableInstance(boolean reference)
   {
      return new MyLine();
   }
   
   
   @Override
   public Object getValue(Object target, String attrName)
   {
      int pos = attrName.indexOf('.');
      String attribute = attrName;
      
      if (pos > 0)
      {
         attribute = attrName.substring(0, pos);
      }

      if (MyLine.PROPERTY_ENDX.equalsIgnoreCase(attribute))
      {
         return ((MyLine) target).getEndX();
      }

      if (MyLine.PROPERTY_ENDY.equalsIgnoreCase(attribute))
      {
         return ((MyLine) target).getEndY();
      }

      if (MyLine.PROPERTY_NAME.equalsIgnoreCase(attribute))
      {
         return ((MyLine) target).getName();
      }

      if (MyLine.PROPERTY_STARTX.equalsIgnoreCase(attribute))
      {
         return ((MyLine) target).getStartX();
      }

      if (MyLine.PROPERTY_STARTY.equalsIgnoreCase(attribute))
      {
         return ((MyLine) target).getStartY();
      }

      if (MyLine.PROPERTY_GRUPPE.equalsIgnoreCase(attribute))
      {
         return ((MyLine) target).getGruppe();
      }
      
      return null;
   }
   
   @Override
   public boolean setValue(Object target, String attrName, Object value, String type)
   {
      if (MyLine.PROPERTY_STARTY.equalsIgnoreCase(attrName))
      {
         ((MyLine) target).setStartY(Integer.parseInt(value.toString()));
         return true;
      }

      if (MyLine.PROPERTY_STARTX.equalsIgnoreCase(attrName))
      {
         ((MyLine) target).setStartX(Integer.parseInt(value.toString()));
         return true;
      }

      if (MyLine.PROPERTY_NAME.equalsIgnoreCase(attrName))
      {
         ((MyLine) target).setName((String) value);
         return true;
      }

      if (MyLine.PROPERTY_ENDY.equalsIgnoreCase(attrName))
      {
         ((MyLine) target).setEndY(Integer.parseInt(value.toString()));
         return true;
      }

      if (MyLine.PROPERTY_ENDX.equalsIgnoreCase(attrName))
      {
         ((MyLine) target).setEndX(Integer.parseInt(value.toString()));
         return true;
      }

      if(SendableEntityCreator.REMOVE_YOU.equals(type)) {
           ((MyLine)target).removeYou();
           return true;
      }
      if (SendableEntityCreator.REMOVE.equals(type) && value != null)
      {
         attrName = attrName + type;
      }

      if (MyLine.PROPERTY_GRUPPE.equalsIgnoreCase(attrName))
      {
         ((MyLine) target).setGruppe((Group) value);
         return true;
      }
      
      return false;
   }
   public static IdMap createIdMap(String sessionID)
   {
      return ha06.util.CreatorCreator.createIdMap(sessionID);
   }
   
   //==========================================================================
      public void removeObject(Object entity)
   {
      ((MyLine) entity).removeYou();
   }
}
