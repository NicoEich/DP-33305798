package ha06.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;
import de.uniks.networkparser.IdMap;
import ha06.Handler;

public class HandlerPOCreator extends PatternObjectCreator
{
   @Override
   public Object getSendableInstance(boolean reference)
   {
      if(reference) {
          return new HandlerPO(new Handler[]{});
      } else {
          return new HandlerPO();
      }
   }
   
   public static IdMap createIdMap(String sessionID) {
      return ha06.util.CreatorCreator.createIdMap(sessionID);
   }
}
