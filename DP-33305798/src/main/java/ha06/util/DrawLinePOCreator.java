package ha06.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;
import de.uniks.networkparser.IdMap;
import ha06.DrawLine;

public class DrawLinePOCreator extends PatternObjectCreator
{
   @Override
   public Object getSendableInstance(boolean reference)
   {
      if(reference) {
          return new DrawLinePO(new DrawLine[]{});
      } else {
          return new DrawLinePO();
      }
   }
   
   public static IdMap createIdMap(String sessionID) {
      return ha06.util.CreatorCreator.createIdMap(sessionID);
   }
}
