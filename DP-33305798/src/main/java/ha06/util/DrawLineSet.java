/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package ha06.util;

import de.uniks.networkparser.list.SimpleSet;
import ha06.DrawLine;
import de.uniks.networkparser.interfaces.Condition;
import java.util.Collection;

public class DrawLineSet extends SimpleSet<DrawLine>
{
	public Class<?> getTypClass() {
		return DrawLine.class;
	}

   public DrawLineSet()
   {
      // empty
   }

   public DrawLineSet(DrawLine... objects)
   {
      for (DrawLine obj : objects)
      {
         this.add(obj);
      }
   }

   public DrawLineSet(Collection<DrawLine> objects)
   {
      this.addAll(objects);
   }

   public static final DrawLineSet EMPTY_SET = new DrawLineSet().withFlag(DrawLineSet.READONLY);


   public DrawLinePO createDrawLinePO()
   {
      return new DrawLinePO(this.toArray(new DrawLine[this.size()]));
   }


   public String getEntryType()
   {
      return "ha06.DrawLine";
   }


   @Override
   public DrawLineSet getNewList(boolean keyValue)
   {
      return new DrawLineSet();
   }


   @SuppressWarnings("unchecked")
   public DrawLineSet with(Object value)
   {
      if (value == null)
      {
         return this;
      }
      else if (value instanceof java.util.Collection)
      {
         this.addAll((Collection<DrawLine>)value);
      }
      else if (value != null)
      {
         this.add((DrawLine) value);
      }
      
      return this;
   }
   
   public DrawLineSet without(DrawLine value)
   {
      this.remove(value);
      return this;
   }

   
   //==========================================================================
   
   public DrawLineSet interprete(String input)
   {
      for (DrawLine obj : this)
      {
         obj.interprete(input);
      }
      return this;
   }

}
