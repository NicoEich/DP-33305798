package ha06.util;

import org.sdmlib.models.pattern.PatternObject;
import ha06.MyLine;
import org.sdmlib.models.pattern.AttributeConstraint;
import org.sdmlib.models.pattern.Pattern;
import ha06.util.GroupPO;
import ha06.Group;
import ha06.util.MyLinePO;

public class MyLinePO extends PatternObject<MyLinePO, MyLine>
{

    public MyLineSet allMatches()
   {
      this.setDoAllMatches(true);
      
      MyLineSet matches = new MyLineSet();

      while (this.getPattern().getHasMatch())
      {
         matches.add((MyLine) this.getCurrentMatch());
         
         this.getPattern().findMatch();
      }
      
      return matches;
   }


   public MyLinePO(){
      newInstance(null);
   }

   public MyLinePO(MyLine... hostGraphObject) {
      if(hostGraphObject==null || hostGraphObject.length<1){
         return ;
      }
      newInstance(null, hostGraphObject);
   }

   public MyLinePO(String modifier)
   {
      this.setModifier(modifier);
   }
   public MyLinePO createEndXCondition(int value)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_ENDX)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public MyLinePO createEndXCondition(int lower, int upper)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_ENDX)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public MyLinePO createEndXAssignment(int value)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_ENDX)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public int getEndX()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((MyLine) getCurrentMatch()).getEndX();
      }
      return 0;
   }
   
   public MyLinePO withEndX(int value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((MyLine) getCurrentMatch()).setEndX(value);
      }
      return this;
   }
   
   public MyLinePO createEndYCondition(int value)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_ENDY)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public MyLinePO createEndYCondition(int lower, int upper)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_ENDY)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public MyLinePO createEndYAssignment(int value)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_ENDY)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public int getEndY()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((MyLine) getCurrentMatch()).getEndY();
      }
      return 0;
   }
   
   public MyLinePO withEndY(int value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((MyLine) getCurrentMatch()).setEndY(value);
      }
      return this;
   }
   
   public MyLinePO createNameCondition(String value)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_NAME)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public MyLinePO createNameCondition(String lower, String upper)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_NAME)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public MyLinePO createNameAssignment(String value)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_NAME)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public String getName()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((MyLine) getCurrentMatch()).getName();
      }
      return null;
   }
   
   public MyLinePO withName(String value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((MyLine) getCurrentMatch()).setName(value);
      }
      return this;
   }
   
   public MyLinePO createStartXCondition(int value)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_STARTX)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public MyLinePO createStartXCondition(int lower, int upper)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_STARTX)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public MyLinePO createStartXAssignment(int value)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_STARTX)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public int getStartX()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((MyLine) getCurrentMatch()).getStartX();
      }
      return 0;
   }
   
   public MyLinePO withStartX(int value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((MyLine) getCurrentMatch()).setStartX(value);
      }
      return this;
   }
   
   public MyLinePO createStartYCondition(int value)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_STARTY)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public MyLinePO createStartYCondition(int lower, int upper)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_STARTY)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public MyLinePO createStartYAssignment(int value)
   {
      new AttributeConstraint()
      .withAttrName(MyLine.PROPERTY_STARTY)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public int getStartY()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((MyLine) getCurrentMatch()).getStartY();
      }
      return 0;
   }
   
   public MyLinePO withStartY(int value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((MyLine) getCurrentMatch()).setStartY(value);
      }
      return this;
   }
   
   public GroupPO createGruppePO()
   {
      GroupPO result = new GroupPO(new Group[]{});
      
      result.setModifier(this.getPattern().getModifier());
      super.hasLink(MyLine.PROPERTY_GRUPPE, result);
      
      return result;
   }

   public GroupPO createGruppePO(String modifier)
   {
      GroupPO result = new GroupPO(new Group[]{});
      
      result.setModifier(modifier);
      super.hasLink(MyLine.PROPERTY_GRUPPE, result);
      
      return result;
   }

   public MyLinePO createGruppeLink(GroupPO tgt)
   {
      return hasLinkConstraint(tgt, MyLine.PROPERTY_GRUPPE);
   }

   public MyLinePO createGruppeLink(GroupPO tgt, String modifier)
   {
      return hasLinkConstraint(tgt, MyLine.PROPERTY_GRUPPE, modifier);
   }

   public Group getGruppe()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((MyLine) this.getCurrentMatch()).getGruppe();
      }
      return null;
   }

}
