package ha06.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;
import de.uniks.networkparser.IdMap;
import ha06.Group;

public class GroupPOCreator extends PatternObjectCreator
{
   @Override
   public Object getSendableInstance(boolean reference)
   {
      if(reference) {
          return new GroupPO(new Group[]{});
      } else {
          return new GroupPO();
      }
   }
   
   public static IdMap createIdMap(String sessionID) {
      return ha06.util.CreatorCreator.createIdMap(sessionID);
   }
}
