/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package ha06.util;

import de.uniks.networkparser.list.SimpleSet;
import ha06.Handler;
import de.uniks.networkparser.interfaces.Condition;
import ha06.DrawLine;
import ha06.util.DrawLineSet;
import ha06.GroupUp;
import ha06.util.GroupUpSet;
import ha06.Clone;
import ha06.util.CloneSet;
import ha06.Undo;
import ha06.util.UndoSet;
import java.util.Collection;

public class HandlerSet extends SimpleSet<Handler>
{
	public Class<?> getTypClass() {
		return Handler.class;
	}

   public HandlerSet()
   {
      // empty
   }

   public HandlerSet(Handler... objects)
   {
      for (Handler obj : objects)
      {
         this.add(obj);
      }
   }

   public HandlerSet(Collection<Handler> objects)
   {
      this.addAll(objects);
   }

   public static final HandlerSet EMPTY_SET = new HandlerSet().withFlag(HandlerSet.READONLY);


   public HandlerPO createHandlerPO()
   {
      return new HandlerPO(this.toArray(new Handler[this.size()]));
   }


   public String getEntryType()
   {
      return "ha06.Handler";
   }


   @Override
   public HandlerSet getNewList(boolean keyValue)
   {
      return new HandlerSet();
   }


   public DrawLineSet instanceOfDrawLine()
   {
      DrawLineSet result = new DrawLineSet();
      
      for(Object obj : this)
      {
         if (obj instanceof DrawLine)
         {
            result.with(obj);
         }
      }
      
      return result;
   }

   public GroupUpSet instanceOfGroupUp()
   {
      GroupUpSet result = new GroupUpSet();
      
      for(Object obj : this)
      {
         if (obj instanceof GroupUp)
         {
            result.with(obj);
         }
      }
      
      return result;
   }

   public CloneSet instanceOfClone()
   {
      CloneSet result = new CloneSet();
      
      for(Object obj : this)
      {
         if (obj instanceof Clone)
         {
            result.with(obj);
         }
      }
      
      return result;
   }

   public UndoSet instanceOfUndo()
   {
      UndoSet result = new UndoSet();
      
      for(Object obj : this)
      {
         if (obj instanceof Undo)
         {
            result.with(obj);
         }
      }
      
      return result;
   }

   @SuppressWarnings("unchecked")
   public HandlerSet with(Object value)
   {
      if (value == null)
      {
         return this;
      }
      else if (value instanceof java.util.Collection)
      {
         this.addAll((Collection<Handler>)value);
      }
      else if (value != null)
      {
         this.add((Handler) value);
      }
      
      return this;
   }
   
   public HandlerSet without(Handler value)
   {
      this.remove(value);
      return this;
   }

   
   //==========================================================================
   
   public HandlerSet interprete(String input)
   {
      for (Handler obj : this)
      {
         obj.interprete(input);
      }
      return this;
   }

}
