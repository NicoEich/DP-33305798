package ha06.util;

import org.sdmlib.models.pattern.PatternObject;
import ha06.GroupUp;

public class GroupUpPO extends PatternObject<GroupUpPO, GroupUp>
{

    public GroupUpSet allMatches()
   {
      this.setDoAllMatches(true);
      
      GroupUpSet matches = new GroupUpSet();

      while (this.getPattern().getHasMatch())
      {
         matches.add((GroupUp) this.getCurrentMatch());
         
         this.getPattern().findMatch();
      }
      
      return matches;
   }


   public GroupUpPO(){
      newInstance(null);
   }

   public GroupUpPO(GroupUp... hostGraphObject) {
      if(hostGraphObject==null || hostGraphObject.length<1){
         return ;
      }
      newInstance(null, hostGraphObject);
   }

   public GroupUpPO(String modifier)
   {
      this.setModifier(modifier);
   }
   
   //==========================================================================
   
   public void interprete(String input)
   {
      if (this.getPattern().getHasMatch())
      {
          ((GroupUp) getCurrentMatch()).interprete(input);
      }
   }

}
