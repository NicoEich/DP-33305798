package ha06.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;
import de.uniks.networkparser.IdMap;
import ha06.GroupUp;

public class GroupUpPOCreator extends PatternObjectCreator
{
   @Override
   public Object getSendableInstance(boolean reference)
   {
      if(reference) {
          return new GroupUpPO(new GroupUp[]{});
      } else {
          return new GroupUpPO();
      }
   }
   
   public static IdMap createIdMap(String sessionID) {
      return ha06.util.CreatorCreator.createIdMap(sessionID);
   }
}
