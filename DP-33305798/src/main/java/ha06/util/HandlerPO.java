package ha06.util;

import org.sdmlib.models.pattern.PatternObject;
import ha06.Handler;

public class HandlerPO extends PatternObject<HandlerPO, Handler>
{

    public HandlerSet allMatches()
   {
      this.setDoAllMatches(true);
      
      HandlerSet matches = new HandlerSet();

      while (this.getPattern().getHasMatch())
      {
         matches.add((Handler) this.getCurrentMatch());
         
         this.getPattern().findMatch();
      }
      
      return matches;
   }


   public HandlerPO(){
      newInstance(null);
   }

   public HandlerPO(Handler... hostGraphObject) {
      if(hostGraphObject==null || hostGraphObject.length<1){
         return ;
      }
      newInstance(null, hostGraphObject);
   }

   public HandlerPO(String modifier)
   {
      this.setModifier(modifier);
   }
   
   //==========================================================================
   
   public void interprete(String input)
   {
      if (this.getPattern().getHasMatch())
      {
          ((Handler) getCurrentMatch()).interprete(input);
      }
   }

}
