/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package ha06.util;

import de.uniks.networkparser.list.SimpleSet;
import ha06.Group;
import de.uniks.networkparser.interfaces.Condition;
import java.util.Collection;
import de.uniks.networkparser.list.ObjectSet;
import java.util.Collections;
import ha06.util.MyLineSet;
import ha06.MyLine;
import ha06.util.GroupSet;

public class GroupSet extends SimpleSet<Group>
{
	public Class<?> getTypClass() {
		return Group.class;
	}

   public GroupSet()
   {
      // empty
   }

   public GroupSet(Group... objects)
   {
      for (Group obj : objects)
      {
         this.add(obj);
      }
   }

   public GroupSet(Collection<Group> objects)
   {
      this.addAll(objects);
   }

   public static final GroupSet EMPTY_SET = new GroupSet().withFlag(GroupSet.READONLY);


   public GroupPO createGroupPO()
   {
      return new GroupPO(this.toArray(new Group[this.size()]));
   }


   public String getEntryType()
   {
      return "ha06.Group";
   }


   @Override
   public GroupSet getNewList(boolean keyValue)
   {
      return new GroupSet();
   }


   @SuppressWarnings("unchecked")
   public GroupSet with(Object value)
   {
      if (value == null)
      {
         return this;
      }
      else if (value instanceof java.util.Collection)
      {
         this.addAll((Collection<Group>)value);
      }
      else if (value != null)
      {
         this.add((Group) value);
      }
      
      return this;
   }
   
   public GroupSet without(Group value)
   {
      this.remove(value);
      return this;
   }


   /**
    * Loop through the current set of Group objects and collect a list of the name attribute values. 
    * 
    * @return List of String objects reachable via name attribute
    */
   public ObjectSet getName()
   {
      ObjectSet result = new ObjectSet();
      
      for (Group obj : this)
      {
         result.add(obj.getName());
      }
      
      return result;
   }


   /**
    * Loop through the current set of Group objects and collect those Group objects where the name attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of Group objects that match the parameter
    */
   public GroupSet createNameCondition(String value)
   {
      GroupSet result = new GroupSet();
      
      for (Group obj : this)
      {
         if (value.equals(obj.getName()))
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Group objects and collect those Group objects where the name attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of Group objects that match the parameter
    */
   public GroupSet createNameCondition(String lower, String upper)
   {
      GroupSet result = new GroupSet();
      
      for (Group obj : this)
      {
         if (lower.compareTo(obj.getName()) <= 0 && obj.getName().compareTo(upper) <= 0)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Group objects and assign value to the name attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of Group objects now with new attribute values.
    */
   public GroupSet withName(String value)
   {
      for (Group obj : this)
      {
         obj.setName(value);
      }
      
      return this;
   }

   /**
    * Loop through the current set of Group objects and collect a set of the MyLine objects reached via linie. 
    * 
    * @return Set of MyLine objects reachable via linie
    */
   public MyLineSet getLinie()
   {
      MyLineSet result = new MyLineSet();
      
      for (Group obj : this)
      {
         result.with(obj.getLinie());
      }
      
      return result;
   }

   /**
    * Loop through the current set of Group objects and collect all contained objects with reference linie pointing to the object passed as parameter. 
    * 
    * @param value The object required as linie neighbor of the collected results. 
    * 
    * @return Set of MyLine objects referring to value via linie
    */
   public GroupSet filterLinie(Object value)
   {
      ObjectSet neighbors = new ObjectSet();

      if (value instanceof Collection)
      {
         neighbors.addAll((Collection<?>) value);
      }
      else
      {
         neighbors.add(value);
      }
      
      GroupSet answer = new GroupSet();
      
      for (Group obj : this)
      {
         if ( ! Collections.disjoint(neighbors, obj.getLinie()))
         {
            answer.add(obj);
         }
      }
      
      return answer;
   }

   /**
    * Loop through current set of ModelType objects and attach the Group object passed as parameter to the Linie attribute of each of it. 
    * 
    * @param value value    * @return The original set of ModelType objects now with the new neighbor attached to their Linie attributes.
    */
   public GroupSet withLinie(MyLine value)
   {
      for (Group obj : this)
      {
         obj.withLinie(value);
      }
      
      return this;
   }

   /**
    * Loop through current set of ModelType objects and remove the Group object passed as parameter from the Linie attribute of each of it. 
    * 
    * @param value value    * @return The original set of ModelType objects now without the old neighbor.
    */
   public GroupSet withoutLinie(MyLine value)
   {
      for (Group obj : this)
      {
         obj.withoutLinie(value);
      }
      
      return this;
   }

   /**
    * Loop through the current set of Group objects and collect a set of the Group objects reached via gruppen. 
    * 
    * @return Set of Group objects reachable via gruppen
    */
   public GroupSet getGruppen()
   {
      GroupSet result = new GroupSet();
      
      for (Group obj : this)
      {
         result.with(obj.getGruppen());
      }
      
      return result;
   }

   /**
    * Loop through the current set of Group objects and collect all contained objects with reference gruppen pointing to the object passed as parameter. 
    * 
    * @param value The object required as gruppen neighbor of the collected results. 
    * 
    * @return Set of Group objects referring to value via gruppen
    */
   public GroupSet filterGruppen(Object value)
   {
      ObjectSet neighbors = new ObjectSet();

      if (value instanceof Collection)
      {
         neighbors.addAll((Collection<?>) value);
      }
      else
      {
         neighbors.add(value);
      }
      
      GroupSet answer = new GroupSet();
      
      for (Group obj : this)
      {
         if ( ! Collections.disjoint(neighbors, obj.getGruppen()))
         {
            answer.add(obj);
         }
      }
      
      return answer;
   }

   /**
    * Follow gruppen reference zero or more times and collect all reachable objects. Detect cycles and deal with them. 
    * 
    * @return Set of Group objects reachable via gruppen transitively (including the start set)
    */
   public GroupSet getGruppenTransitive()
   {
      GroupSet todo = new GroupSet().with(this);
      
      GroupSet result = new GroupSet();
      
      while ( ! todo.isEmpty())
      {
         Group current = todo.first();
         
         todo.remove(current);
         
         if ( ! result.contains(current))
         {
            result.add(current);
            
            todo.with(current.getGruppen()).minus(result);
         }
      }
      
      return result;
   }

   /**
    * Loop through current set of ModelType objects and attach the Group object passed as parameter to the Gruppen attribute of each of it. 
    * 
    * @param value value    * @return The original set of ModelType objects now with the new neighbor attached to their Gruppen attributes.
    */
   public GroupSet withGruppen(Group value)
   {
      for (Group obj : this)
      {
         obj.withGruppen(value);
      }
      
      return this;
   }

   /**
    * Loop through current set of ModelType objects and remove the Group object passed as parameter from the Gruppen attribute of each of it. 
    * 
    * @param value value    * @return The original set of ModelType objects now without the old neighbor.
    */
   public GroupSet withoutGruppen(Group value)
   {
      for (Group obj : this)
      {
         obj.withoutGruppen(value);
      }
      
      return this;
   }

}
