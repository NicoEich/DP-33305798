package ha06.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;
import de.uniks.networkparser.IdMap;
import ha06.Undo;

public class UndoPOCreator extends PatternObjectCreator
{
   @Override
   public Object getSendableInstance(boolean reference)
   {
      if(reference) {
          return new UndoPO(new Undo[]{});
      } else {
          return new UndoPO();
      }
   }
   
   public static IdMap createIdMap(String sessionID) {
      return ha06.util.CreatorCreator.createIdMap(sessionID);
   }
}
