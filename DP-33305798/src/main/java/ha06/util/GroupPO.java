package ha06.util;

import org.sdmlib.models.pattern.PatternObject;
import ha06.Group;
import org.sdmlib.models.pattern.AttributeConstraint;
import org.sdmlib.models.pattern.Pattern;
import ha06.util.MyLinePO;
import ha06.MyLine;
import ha06.util.GroupPO;
import ha06.util.MyLineSet;
import ha06.util.GroupSet;

public class GroupPO extends PatternObject<GroupPO, Group>
{

    public GroupSet allMatches()
   {
      this.setDoAllMatches(true);
      
      GroupSet matches = new GroupSet();

      while (this.getPattern().getHasMatch())
      {
         matches.add((Group) this.getCurrentMatch());
         
         this.getPattern().findMatch();
      }
      
      return matches;
   }


   public GroupPO(){
      newInstance(null);
   }

   public GroupPO(Group... hostGraphObject) {
      if(hostGraphObject==null || hostGraphObject.length<1){
         return ;
      }
      newInstance(null, hostGraphObject);
   }

   public GroupPO(String modifier)
   {
      this.setModifier(modifier);
   }
   public GroupPO createNameCondition(String value)
   {
      new AttributeConstraint()
      .withAttrName(Group.PROPERTY_NAME)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public GroupPO createNameCondition(String lower, String upper)
   {
      new AttributeConstraint()
      .withAttrName(Group.PROPERTY_NAME)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public GroupPO createNameAssignment(String value)
   {
      new AttributeConstraint()
      .withAttrName(Group.PROPERTY_NAME)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public String getName()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Group) getCurrentMatch()).getName();
      }
      return null;
   }
   
   public GroupPO withName(String value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((Group) getCurrentMatch()).setName(value);
      }
      return this;
   }
   
   public MyLinePO createLiniePO()
   {
      MyLinePO result = new MyLinePO(new MyLine[]{});
      
      result.setModifier(this.getPattern().getModifier());
      super.hasLink(Group.PROPERTY_LINIE, result);
      
      return result;
   }

   public MyLinePO createLiniePO(String modifier)
   {
      MyLinePO result = new MyLinePO(new MyLine[]{});
      
      result.setModifier(modifier);
      super.hasLink(Group.PROPERTY_LINIE, result);
      
      return result;
   }

   public GroupPO createLinieLink(MyLinePO tgt)
   {
      return hasLinkConstraint(tgt, Group.PROPERTY_LINIE);
   }

   public GroupPO createLinieLink(MyLinePO tgt, String modifier)
   {
      return hasLinkConstraint(tgt, Group.PROPERTY_LINIE, modifier);
   }

   public MyLineSet getLinie()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Group) this.getCurrentMatch()).getLinie();
      }
      return null;
   }

   public GroupPO createGruppenPO()
   {
      GroupPO result = new GroupPO(new Group[]{});
      
      result.setModifier(this.getPattern().getModifier());
      super.hasLink(Group.PROPERTY_GRUPPEN, result);
      
      return result;
   }

   public GroupPO createGruppenPO(String modifier)
   {
      GroupPO result = new GroupPO(new Group[]{});
      
      result.setModifier(modifier);
      super.hasLink(Group.PROPERTY_GRUPPEN, result);
      
      return result;
   }

   public GroupPO createGruppenLink(GroupPO tgt)
   {
      return hasLinkConstraint(tgt, Group.PROPERTY_GRUPPEN);
   }

   public GroupPO createGruppenLink(GroupPO tgt, String modifier)
   {
      return hasLinkConstraint(tgt, Group.PROPERTY_GRUPPEN, modifier);
   }

   public GroupSet getGruppen()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Group) this.getCurrentMatch()).getGruppen();
      }
      return null;
   }

}
