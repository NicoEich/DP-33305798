/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package ha06.util;

import de.uniks.networkparser.list.SimpleSet;
import ha06.MyLine;
import de.uniks.networkparser.interfaces.Condition;
import java.util.Collection;
import de.uniks.networkparser.list.NumberList;
import de.uniks.networkparser.list.ObjectSet;
import ha06.util.GroupSet;
import ha06.Group;

public class MyLineSet extends SimpleSet<MyLine>
{
	public Class<?> getTypClass() {
		return MyLine.class;
	}

   public MyLineSet()
   {
      // empty
   }

   public MyLineSet(MyLine... objects)
   {
      for (MyLine obj : objects)
      {
         this.add(obj);
      }
   }

   public MyLineSet(Collection<MyLine> objects)
   {
      this.addAll(objects);
   }

   public static final MyLineSet EMPTY_SET = new MyLineSet().withFlag(MyLineSet.READONLY);


   public MyLinePO createMyLinePO()
   {
      return new MyLinePO(this.toArray(new MyLine[this.size()]));
   }


   public String getEntryType()
   {
      return "ha06.MyLine";
   }


   @Override
   public MyLineSet getNewList(boolean keyValue)
   {
      return new MyLineSet();
   }


   @SuppressWarnings("unchecked")
   public MyLineSet with(Object value)
   {
      if (value == null)
      {
         return this;
      }
      else if (value instanceof java.util.Collection)
      {
         this.addAll((Collection<MyLine>)value);
      }
      else if (value != null)
      {
         this.add((MyLine) value);
      }
      
      return this;
   }
   
   public MyLineSet without(MyLine value)
   {
      this.remove(value);
      return this;
   }


   /**
    * Loop through the current set of MyLine objects and collect a list of the endX attribute values. 
    * 
    * @return List of int objects reachable via endX attribute
    */
   public NumberList getEndX()
   {
      NumberList result = new NumberList();
      
      for (MyLine obj : this)
      {
         result.add(obj.getEndX());
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and collect those MyLine objects where the endX attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of MyLine objects that match the parameter
    */
   public MyLineSet createEndXCondition(int value)
   {
      MyLineSet result = new MyLineSet();
      
      for (MyLine obj : this)
      {
         if (value == obj.getEndX())
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and collect those MyLine objects where the endX attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of MyLine objects that match the parameter
    */
   public MyLineSet createEndXCondition(int lower, int upper)
   {
      MyLineSet result = new MyLineSet();
      
      for (MyLine obj : this)
      {
         if (lower <= obj.getEndX() && obj.getEndX() <= upper)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and assign value to the endX attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of MyLine objects now with new attribute values.
    */
   public MyLineSet withEndX(int value)
   {
      for (MyLine obj : this)
      {
         obj.setEndX(value);
      }
      
      return this;
   }


   /**
    * Loop through the current set of MyLine objects and collect a list of the endY attribute values. 
    * 
    * @return List of int objects reachable via endY attribute
    */
   public NumberList getEndY()
   {
      NumberList result = new NumberList();
      
      for (MyLine obj : this)
      {
         result.add(obj.getEndY());
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and collect those MyLine objects where the endY attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of MyLine objects that match the parameter
    */
   public MyLineSet createEndYCondition(int value)
   {
      MyLineSet result = new MyLineSet();
      
      for (MyLine obj : this)
      {
         if (value == obj.getEndY())
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and collect those MyLine objects where the endY attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of MyLine objects that match the parameter
    */
   public MyLineSet createEndYCondition(int lower, int upper)
   {
      MyLineSet result = new MyLineSet();
      
      for (MyLine obj : this)
      {
         if (lower <= obj.getEndY() && obj.getEndY() <= upper)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and assign value to the endY attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of MyLine objects now with new attribute values.
    */
   public MyLineSet withEndY(int value)
   {
      for (MyLine obj : this)
      {
         obj.setEndY(value);
      }
      
      return this;
   }


   /**
    * Loop through the current set of MyLine objects and collect a list of the name attribute values. 
    * 
    * @return List of String objects reachable via name attribute
    */
   public ObjectSet getName()
   {
      ObjectSet result = new ObjectSet();
      
      for (MyLine obj : this)
      {
         result.add(obj.getName());
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and collect those MyLine objects where the name attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of MyLine objects that match the parameter
    */
   public MyLineSet createNameCondition(String value)
   {
      MyLineSet result = new MyLineSet();
      
      for (MyLine obj : this)
      {
         if (value.equals(obj.getName()))
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and collect those MyLine objects where the name attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of MyLine objects that match the parameter
    */
   public MyLineSet createNameCondition(String lower, String upper)
   {
      MyLineSet result = new MyLineSet();
      
      for (MyLine obj : this)
      {
         if (lower.compareTo(obj.getName()) <= 0 && obj.getName().compareTo(upper) <= 0)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and assign value to the name attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of MyLine objects now with new attribute values.
    */
   public MyLineSet withName(String value)
   {
      for (MyLine obj : this)
      {
         obj.setName(value);
      }
      
      return this;
   }


   /**
    * Loop through the current set of MyLine objects and collect a list of the startX attribute values. 
    * 
    * @return List of int objects reachable via startX attribute
    */
   public NumberList getStartX()
   {
      NumberList result = new NumberList();
      
      for (MyLine obj : this)
      {
         result.add(obj.getStartX());
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and collect those MyLine objects where the startX attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of MyLine objects that match the parameter
    */
   public MyLineSet createStartXCondition(int value)
   {
      MyLineSet result = new MyLineSet();
      
      for (MyLine obj : this)
      {
         if (value == obj.getStartX())
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and collect those MyLine objects where the startX attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of MyLine objects that match the parameter
    */
   public MyLineSet createStartXCondition(int lower, int upper)
   {
      MyLineSet result = new MyLineSet();
      
      for (MyLine obj : this)
      {
         if (lower <= obj.getStartX() && obj.getStartX() <= upper)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and assign value to the startX attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of MyLine objects now with new attribute values.
    */
   public MyLineSet withStartX(int value)
   {
      for (MyLine obj : this)
      {
         obj.setStartX(value);
      }
      
      return this;
   }


   /**
    * Loop through the current set of MyLine objects and collect a list of the startY attribute values. 
    * 
    * @return List of int objects reachable via startY attribute
    */
   public NumberList getStartY()
   {
      NumberList result = new NumberList();
      
      for (MyLine obj : this)
      {
         result.add(obj.getStartY());
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and collect those MyLine objects where the startY attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of MyLine objects that match the parameter
    */
   public MyLineSet createStartYCondition(int value)
   {
      MyLineSet result = new MyLineSet();
      
      for (MyLine obj : this)
      {
         if (value == obj.getStartY())
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and collect those MyLine objects where the startY attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of MyLine objects that match the parameter
    */
   public MyLineSet createStartYCondition(int lower, int upper)
   {
      MyLineSet result = new MyLineSet();
      
      for (MyLine obj : this)
      {
         if (lower <= obj.getStartY() && obj.getStartY() <= upper)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of MyLine objects and assign value to the startY attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of MyLine objects now with new attribute values.
    */
   public MyLineSet withStartY(int value)
   {
      for (MyLine obj : this)
      {
         obj.setStartY(value);
      }
      
      return this;
   }

   /**
    * Loop through the current set of MyLine objects and collect a set of the Group objects reached via gruppe. 
    * 
    * @return Set of Group objects reachable via gruppe
    */
   public GroupSet getGruppe()
   {
      GroupSet result = new GroupSet();
      
      for (MyLine obj : this)
      {
         result.with(obj.getGruppe());
      }
      
      return result;
   }

   /**
    * Loop through the current set of MyLine objects and collect all contained objects with reference gruppe pointing to the object passed as parameter. 
    * 
    * @param value The object required as gruppe neighbor of the collected results. 
    * 
    * @return Set of Group objects referring to value via gruppe
    */
   public MyLineSet filterGruppe(Object value)
   {
      ObjectSet neighbors = new ObjectSet();

      if (value instanceof Collection)
      {
         neighbors.addAll((Collection<?>) value);
      }
      else
      {
         neighbors.add(value);
      }
      
      MyLineSet answer = new MyLineSet();
      
      for (MyLine obj : this)
      {
         if (neighbors.contains(obj.getGruppe()) || (neighbors.isEmpty() && obj.getGruppe() == null))
         {
            answer.add(obj);
         }
      }
      
      return answer;
   }

   /**
    * Loop through current set of ModelType objects and attach the MyLine object passed as parameter to the Gruppe attribute of each of it. 
    * 
    * @param value value    * @return The original set of ModelType objects now with the new neighbor attached to their Gruppe attributes.
    */
   public MyLineSet withGruppe(Group value)
   {
      for (MyLine obj : this)
      {
         obj.withGruppe(value);
      }
      
      return this;
   }

}
