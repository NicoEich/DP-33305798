package ha06.util;

import org.sdmlib.models.pattern.PatternObject;
import ha06.Undo;

public class UndoPO extends PatternObject<UndoPO, Undo>
{

    public UndoSet allMatches()
   {
      this.setDoAllMatches(true);
      
      UndoSet matches = new UndoSet();

      while (this.getPattern().getHasMatch())
      {
         matches.add((Undo) this.getCurrentMatch());
         
         this.getPattern().findMatch();
      }
      
      return matches;
   }


   public UndoPO(){
      newInstance(null);
   }

   public UndoPO(Undo... hostGraphObject) {
      if(hostGraphObject==null || hostGraphObject.length<1){
         return ;
      }
      newInstance(null, hostGraphObject);
   }

   public UndoPO(String modifier)
   {
      this.setModifier(modifier);
   }
   
   //==========================================================================
   
   public void interprete(String input)
   {
      if (this.getPattern().getHasMatch())
      {
          ((Undo) getCurrentMatch()).interprete(input);
      }
   }

}
