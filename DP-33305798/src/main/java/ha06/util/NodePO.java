package ha06.util;

import org.sdmlib.models.pattern.PatternObject;
import ha06.Node;
import org.sdmlib.models.pattern.AttributeConstraint;
import org.sdmlib.models.pattern.Pattern;

public class NodePO extends PatternObject<NodePO, Node>
{

    public NodeSet allMatches()
   {
      this.setDoAllMatches(true);
      
      NodeSet matches = new NodeSet();

      while (this.getPattern().getHasMatch())
      {
         matches.add((Node) this.getCurrentMatch());
         
         this.getPattern().findMatch();
      }
      
      return matches;
   }


   public NodePO(){
      newInstance(null);
   }

   public NodePO(Node... hostGraphObject) {
      if(hostGraphObject==null || hostGraphObject.length<1){
         return ;
      }
      newInstance(null, hostGraphObject);
   }

   public NodePO(String modifier)
   {
      this.setModifier(modifier);
   }
   public NodePO createNameCondition(String value)
   {
      new AttributeConstraint()
      .withAttrName(Node.PROPERTY_NAME)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public NodePO createNameCondition(String lower, String upper)
   {
      new AttributeConstraint()
      .withAttrName(Node.PROPERTY_NAME)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public NodePO createNameAssignment(String value)
   {
      new AttributeConstraint()
      .withAttrName(Node.PROPERTY_NAME)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public String getName()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Node) getCurrentMatch()).getName();
      }
      return null;
   }
   
   public NodePO withName(String value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((Node) getCurrentMatch()).setName(value);
      }
      return this;
   }
   
}
