package ha06.util;

import org.sdmlib.models.pattern.PatternObject;
import ha06.DrawLine;

public class DrawLinePO extends PatternObject<DrawLinePO, DrawLine>
{

    public DrawLineSet allMatches()
   {
      this.setDoAllMatches(true);
      
      DrawLineSet matches = new DrawLineSet();

      while (this.getPattern().getHasMatch())
      {
         matches.add((DrawLine) this.getCurrentMatch());
         
         this.getPattern().findMatch();
      }
      
      return matches;
   }


   public DrawLinePO(){
      newInstance(null);
   }

   public DrawLinePO(DrawLine... hostGraphObject) {
      if(hostGraphObject==null || hostGraphObject.length<1){
         return ;
      }
      newInstance(null, hostGraphObject);
   }

   public DrawLinePO(String modifier)
   {
      this.setModifier(modifier);
   }
   
   //==========================================================================
   
   public void interprete(String input)
   {
      if (this.getPattern().getHasMatch())
      {
          ((DrawLine) getCurrentMatch()).interprete(input);
      }
   }

}
