package ha06.util;

import de.uniks.networkparser.IdMap;

class CreatorCreator{

   public static IdMap createIdMap(String session)
   {
      IdMap jsonIdMap = new IdMap().withSession(session);
      jsonIdMap.with(new CloneCreator());
      jsonIdMap.with(new ClonePOCreator());
      jsonIdMap.with(new DrawLineCreator());
      jsonIdMap.with(new DrawLinePOCreator());
      jsonIdMap.with(new GroupCreator());
      jsonIdMap.with(new GroupPOCreator());
      jsonIdMap.with(new GroupUpCreator());
      jsonIdMap.with(new GroupUpPOCreator());
      jsonIdMap.with(new HandlerCreator());
      jsonIdMap.with(new HandlerPOCreator());
      jsonIdMap.with(new MyLineCreator());
      jsonIdMap.with(new MyLinePOCreator());
      jsonIdMap.with(new NodeCreator());
      jsonIdMap.with(new NodePOCreator());
      jsonIdMap.with(new UndoCreator());
      jsonIdMap.with(new UndoPOCreator());
      return jsonIdMap;
   }
}
