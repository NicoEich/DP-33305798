/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package ha06.util;

import de.uniks.networkparser.interfaces.AggregatedEntityCreator;
import ha06.Group;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.networkparser.IdMap;
import ha06.Node;
import ha06.MyLine;

public class GroupCreator implements AggregatedEntityCreator
{
   public static final GroupCreator it = new GroupCreator();
   
   private final String[] properties = new String[]
   {
      Node.PROPERTY_NAME,
      Group.PROPERTY_LINIE,
      Group.PROPERTY_GRUPPEN,
   };
   
   private final String[] upProperties = new String[]
   {
   };
   
   private final String[] downProperties = new String[]
   {
   };
   
   @Override
   public String[] getProperties()
   {
      return properties;
   }
   
   @Override
   public String[] getUpProperties()
   {
      return upProperties;
   }
   
   @Override
   public String[] getDownProperties()
   {
      return downProperties;
   }
   
   @Override
   public Object getSendableInstance(boolean reference)
   {
      return new Group();
   }
   
   
   @Override
   public Object getValue(Object target, String attrName)
   {
      int pos = attrName.indexOf('.');
      String attribute = attrName;
      
      if (pos > 0)
      {
         attribute = attrName.substring(0, pos);
      }

      if (Node.PROPERTY_NAME.equalsIgnoreCase(attribute))
      {
         return ((Node) target).getName();
      }

      if (Group.PROPERTY_LINIE.equalsIgnoreCase(attribute))
      {
         return ((Group) target).getLinie();
      }

      if (Group.PROPERTY_GRUPPEN.equalsIgnoreCase(attribute))
      {
         return ((Group) target).getGruppen();
      }
      
      return null;
   }
   
   @Override
   public boolean setValue(Object target, String attrName, Object value, String type)
   {
      if (Node.PROPERTY_NAME.equalsIgnoreCase(attrName))
      {
         ((Node) target).setName((String) value);
         return true;
      }

      if(SendableEntityCreator.REMOVE_YOU.equals(type)) {
           ((Group)target).removeYou();
           return true;
      }
      if (SendableEntityCreator.REMOVE.equals(type) && value != null)
      {
         attrName = attrName + type;
      }

      if (Group.PROPERTY_LINIE.equalsIgnoreCase(attrName))
      {
         ((Group) target).withLinie((MyLine) value);
         return true;
      }
      
      if ((Group.PROPERTY_LINIE + SendableEntityCreator.REMOVE).equalsIgnoreCase(attrName))
      {
         ((Group) target).withoutLinie((MyLine) value);
         return true;
      }

      if (Group.PROPERTY_GRUPPEN.equalsIgnoreCase(attrName))
      {
         ((Group) target).withGruppen((Group) value);
         return true;
      }
      
      if ((Group.PROPERTY_GRUPPEN + SendableEntityCreator.REMOVE).equalsIgnoreCase(attrName))
      {
         ((Group) target).withoutGruppen((Group) value);
         return true;
      }
      
      return false;
   }
   public static IdMap createIdMap(String sessionID)
   {
      return ha06.util.CreatorCreator.createIdMap(sessionID);
   }
   
   //==========================================================================
      public void removeObject(Object entity)
   {
      ((Group) entity).removeYou();
   }
}
