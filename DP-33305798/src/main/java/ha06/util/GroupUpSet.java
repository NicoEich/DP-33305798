/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package ha06.util;

import de.uniks.networkparser.list.SimpleSet;
import ha06.GroupUp;
import de.uniks.networkparser.interfaces.Condition;
import java.util.Collection;

public class GroupUpSet extends SimpleSet<GroupUp>
{
	public Class<?> getTypClass() {
		return GroupUp.class;
	}

   public GroupUpSet()
   {
      // empty
   }

   public GroupUpSet(GroupUp... objects)
   {
      for (GroupUp obj : objects)
      {
         this.add(obj);
      }
   }

   public GroupUpSet(Collection<GroupUp> objects)
   {
      this.addAll(objects);
   }

   public static final GroupUpSet EMPTY_SET = new GroupUpSet().withFlag(GroupUpSet.READONLY);


   public GroupUpPO createGroupUpPO()
   {
      return new GroupUpPO(this.toArray(new GroupUp[this.size()]));
   }


   public String getEntryType()
   {
      return "ha06.GroupUp";
   }


   @Override
   public GroupUpSet getNewList(boolean keyValue)
   {
      return new GroupUpSet();
   }


   @SuppressWarnings("unchecked")
   public GroupUpSet with(Object value)
   {
      if (value == null)
      {
         return this;
      }
      else if (value instanceof java.util.Collection)
      {
         this.addAll((Collection<GroupUp>)value);
      }
      else if (value != null)
      {
         this.add((GroupUp) value);
      }
      
      return this;
   }
   
   public GroupUpSet without(GroupUp value)
   {
      this.remove(value);
      return this;
   }

   
   //==========================================================================
   
   public GroupUpSet interprete(String input)
   {
      for (GroupUp obj : this)
      {
         obj.interprete(input);
      }
      return this;
   }

}
