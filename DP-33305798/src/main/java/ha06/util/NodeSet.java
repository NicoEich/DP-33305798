/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package ha06.util;

import de.uniks.networkparser.list.SimpleSet;
import ha06.Node;
import de.uniks.networkparser.interfaces.Condition;
import ha06.MyLine;
import ha06.util.MyLineSet;
import ha06.Group;
import ha06.util.GroupSet;
import java.util.Collection;
import de.uniks.networkparser.list.ObjectSet;

public class NodeSet extends SimpleSet<Node>
{
	public Class<?> getTypClass() {
		return Node.class;
	}

   public NodeSet()
   {
      // empty
   }

   public NodeSet(Node... objects)
   {
      for (Node obj : objects)
      {
         this.add(obj);
      }
   }

   public NodeSet(Collection<Node> objects)
   {
      this.addAll(objects);
   }

   public static final NodeSet EMPTY_SET = new NodeSet().withFlag(NodeSet.READONLY);


   public NodePO createNodePO()
   {
      return new NodePO(this.toArray(new Node[this.size()]));
   }


   public String getEntryType()
   {
      return "ha06.Node";
   }


   @Override
   public NodeSet getNewList(boolean keyValue)
   {
      return new NodeSet();
   }


   public MyLineSet instanceOfMyLine()
   {
      MyLineSet result = new MyLineSet();
      
      for(Object obj : this)
      {
         if (obj instanceof MyLine)
         {
            result.with(obj);
         }
      }
      
      return result;
   }

   public GroupSet instanceOfGroup()
   {
      GroupSet result = new GroupSet();
      
      for(Object obj : this)
      {
         if (obj instanceof Group)
         {
            result.with(obj);
         }
      }
      
      return result;
   }

   @SuppressWarnings("unchecked")
   public NodeSet with(Object value)
   {
      if (value == null)
      {
         return this;
      }
      else if (value instanceof java.util.Collection)
      {
         this.addAll((Collection<Node>)value);
      }
      else if (value != null)
      {
         this.add((Node) value);
      }
      
      return this;
   }
   
   public NodeSet without(Node value)
   {
      this.remove(value);
      return this;
   }


   /**
    * Loop through the current set of Node objects and collect a list of the name attribute values. 
    * 
    * @return List of String objects reachable via name attribute
    */
   public ObjectSet getName()
   {
      ObjectSet result = new ObjectSet();
      
      for (Node obj : this)
      {
         result.add(obj.getName());
      }
      
      return result;
   }


   /**
    * Loop through the current set of Node objects and collect those Node objects where the name attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of Node objects that match the parameter
    */
   public NodeSet createNameCondition(String value)
   {
      NodeSet result = new NodeSet();
      
      for (Node obj : this)
      {
         if (value.equals(obj.getName()))
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Node objects and collect those Node objects where the name attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of Node objects that match the parameter
    */
   public NodeSet createNameCondition(String lower, String upper)
   {
      NodeSet result = new NodeSet();
      
      for (Node obj : this)
      {
         if (lower.compareTo(obj.getName()) <= 0 && obj.getName().compareTo(upper) <= 0)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Node objects and assign value to the name attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of Node objects now with new attribute values.
    */
   public NodeSet withName(String value)
   {
      for (Node obj : this)
      {
         obj.setName(value);
      }
      
      return this;
   }

}
