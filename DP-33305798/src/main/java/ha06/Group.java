/*
   Copyright (c) 2018 Nico
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package ha06;

import ha06.Node;
import de.uniks.networkparser.EntityUtil;
import ha06.util.MyLineSet;
import ha06.MyLine;
import ha06.util.GroupSet;

public  class Group extends Node
{

   
   //==========================================================================
   
   @Override
   public void removeYou()
   {
      withoutLinie(this.getLinie().toArray(new MyLine[this.getLinie().size()]));
      withoutGruppen(this.getGruppen().toArray(new Group[this.getGruppen().size()]));
      firePropertyChange("REMOVE_YOU", this, null);
   }

   
   //==========================================================================
   
   public static final String PROPERTY_NAME = "name";
   
   private String name;

   public String getName()
   {
      return this.name;
   }
   
   public void setName(String value)
   {
      if ( ! EntityUtil.stringEquals(this.name, value)) {
      
         String oldValue = this.name;
         this.name = value;
         this.firePropertyChange(PROPERTY_NAME, oldValue, value);
      }
   }
   
   public Group withName(String value)
   {
      setName(value);
      return this;
   } 


   @Override
   public String toString()
   {
      StringBuilder result = new StringBuilder();
      
      result.append(" ").append(this.getName());
      return result.substring(1);
   }


   
   /********************************************************************
    * <pre>
    *              one                       many
    * Group ----------------------------------- MyLine
    *              gruppe                   linie
    * </pre>
    */
   
   public static final String PROPERTY_LINIE = "linie";

   private MyLineSet linie = null;
   
   public MyLineSet getLinie()
   {
      if (this.linie == null)
      {
         return MyLineSet.EMPTY_SET;
      }
   
      return this.linie;
   }

   public Group withLinie(MyLine... value)
   {
      if(value==null){
         return this;
      }
      for (MyLine item : value)
      {
         if (item != null)
         {
            if (this.linie == null)
            {
               this.linie = new MyLineSet();
            }
            
            boolean changed = this.linie.add (item);

            if (changed)
            {
               item.withGruppe(this);
               firePropertyChange(PROPERTY_LINIE, null, item);
            }
         }
      }
      return this;
   } 

   public Group withoutLinie(MyLine... value)
   {
      for (MyLine item : value)
      {
         if ((this.linie != null) && (item != null))
         {
            if (this.linie.remove(item))
            {
               item.setGruppe(null);
               firePropertyChange(PROPERTY_LINIE, item, null);
            }
         }
      }
      return this;
   }

   public MyLine createLinie()
   {
      MyLine value = new MyLine();
      withLinie(value);
      return value;
   } 

   
   /********************************************************************
    * <pre>
    *              many                       many
    * Group ----------------------------------- Group
    *              gruppen                   gruppen
    * </pre>
    */
   
   public static final String PROPERTY_GRUPPEN = "gruppen";

   private GroupSet gruppen = null;
   
   public GroupSet getGruppen()
   {
      if (this.gruppen == null)
      {
         return GroupSet.EMPTY_SET;
      }
   
      return this.gruppen;
   }
   public GroupSet getGruppenTransitive()
   {
      GroupSet result = new GroupSet().with(this);
      return result.getGruppenTransitive();
   }


   public Group withGruppen(Group... value)
   {
      if(value==null){
         return this;
      }
      for (Group item : value)
      {
         if (item != null)
         {
            if (this.gruppen == null)
            {
               this.gruppen = new GroupSet();
            }
            
            boolean changed = this.gruppen.add (item);

            if (changed)
            {
               item.withGruppen(this);
               firePropertyChange(PROPERTY_GRUPPEN, null, item);
            }
         }
      }
      return this;
   } 

   public Group withoutGruppen(Group... value)
   {
      for (Group item : value)
      {
         if ((this.gruppen != null) && (item != null))
         {
            if (this.gruppen.remove(item))
            {
               item.withoutGruppen(this);
               firePropertyChange(PROPERTY_GRUPPEN, item, null);
            }
         }
      }
      return this;
   }

   public Group createGruppen()
   {
      Group value = new Group();
      withGruppen(value);
      return value;
   } 
}
