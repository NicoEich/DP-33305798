package ha01;

public class DeliveryService {

	 static Caterer caterer = new Caterer();    // Pointer on Caterer 

	public static void deliver(int fNo, String address) {							// could make a caterer parameter for this method instead of a global variable 
		
		caterer.deliver(fNo, address);
		
	}
	
	public void setCaterer(Caterer c) {		// change Pointer
	
		caterer = c ;
	}

}
