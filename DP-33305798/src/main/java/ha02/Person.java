package ha02;

public class Person  extends Node{

	private String position;
	
	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public Person( String name , String position , int budget){
		
		this.position = position;
		this.setName(name);
	}
	
	public Person(String name , String position){
		
		this.position = position;
		this.setName(name);
	}
	
	
	public Person(String position){
		
		this.position = position;
	}
	
	
	public String getPosition() {
		
		return position;
	}
	
	public void setPosition(String position) {
		
		this.position = position ;
	}
}
