package ha02;

import java.util.ArrayList;

public class Node {

	private ArrayList<Node> children = new ArrayList<Node>();
	private String name;   // everything has a name so i can use it in Node 
	private int budget = 0; // everything has a budget so i can use it in Node 
	
	public void accept(Visitor v) {
		
	}
	
	public void addChild(Node n) {
		
		children.add(n);
	}
	
	public void addChildren(ArrayList<Node> n) {
		
		children.addAll(n);
	}
	
	public void removeChild(Node n) {
		
		children.remove(n);
	}
	
	public void removeChildren(ArrayList<Node> n) {
		
		children.removeAll(n);
		
	}
	
	
	// Getters & Setters 
	
	public ArrayList<Node> getChildren() {
		
		return children;
	}
	
	public String getName() {
		
		return name;
	}
	
	public void setName(String s) {
		
		name = s ;
	}
	

	public int getBudget() {
		return budget;
	}

	public void setBudget(int budget) {
		this.budget = budget;
	}

}
