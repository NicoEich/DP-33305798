package ha02;

public class Fakultaet extends Node {

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public Fakultaet(String s, int budget){
		this.setBudget(budget);
		this.setName(s);
	}
}
