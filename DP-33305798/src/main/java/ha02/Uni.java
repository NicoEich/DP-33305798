package ha02;

public class Uni extends Node {
	
	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public Uni(String s , int budget){
		this.setBudget(budget);
		this.setName(s);
	}
	
	
}
