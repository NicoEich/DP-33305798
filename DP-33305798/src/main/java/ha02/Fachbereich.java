package ha02;

public class Fachbereich extends Node{
	
	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	
	public Fachbereich(String s, int budget){
		this.setBudget(budget);
		this.setName(s);
	}
	

}
