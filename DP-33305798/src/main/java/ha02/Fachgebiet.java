package ha02;

public class Fachgebiet  extends Node {
	
	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public Fachgebiet(String s, int budget){
		
		this.setBudget(budget);
		this.setName(s);
	}
	

}
