package ha02;

public class Visitor {

	private int sumBudget = 0;
	
	public void visit(Node n) {
		
		for( Node c : n.getChildren()) {
			
			c.accept(this);
			//this.visit(c);					// wont work so i used a workaround 
			
		}
	}
	
	
	public void visit(Fachgebiet f ) {
		
		sumBudget += f.getBudget();
		
		for( Node c : f.getChildren()) {
			
			
			if(!c.getChildren().isEmpty()) {    // if c got children visit them 
				
				//this.visit(c);      			 wont work so i used a workaround 
				
				c.accept(this);
			}
			
			sumBudget+= c.getBudget();			
							
		}		
	}
	
	
	public int getSumBudget() {
		return sumBudget;
	}
}
