package ha02;

public class Institut extends Node {

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public Institut(String s, int budget){
		this.setBudget(budget);
		this.setName(s);
	}
}
