package ha1Test;

import static org.junit.Assert.*;

import org.junit.Test;

import ha01.BurgerBude;
import ha01.ChinaMan;
import ha01.DeliveryService;
import ha01.DoenerLaden;
import ha01.PizzaShop;

/**
 * @author Xavier Ngansop
 * HA1: 06/10
 * -2 Die Klasse Carterer muss entweder eine abstrakte Klasse oder eine Interface
 * -2 Carterer als static zu definieren ist nicht gut. es sollte die Möglichkeit geben
 *  mehrere DeliveryService Instanzen zu erstellen.
 * Das ist kein Junit-test. Es fehlen Assertions. Nächstes mal ziehe ich Punkte dafür ab.
 */
public class DeliveryServiceTest {									// not inside of the test

	@Test
	public void test() {
		
		DeliveryService ds = new DeliveryService();
		PizzaShop p = new PizzaShop();
		BurgerBude b = new BurgerBude();
		DoenerLaden d = new DoenerLaden();
		ChinaMan c = new ChinaMan();
		
		ds.setCaterer(p);											// Change pointer on PizzaShop object
		
		DeliveryService.deliver(42, "WilliAlle73");
		
		ds.setCaterer(b);											// Change pointer on BurgerBude object
		
		DeliveryService.deliver(42, "WilliAlle73");
		
		ds.setCaterer(d);											// Change pointer on DoenerBude object
		
		DeliveryService.deliver(42, "WilliAlle73");					
		
		ds.setCaterer(c);											// Change pointer on China object
		
		DeliveryService.deliver(42, "WilliAlle73");	
		
		
	}

}
