package ha02Test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ha02.Fachbereich;
import ha02.Fachgebiet;
import ha02.Fakultaet;
import ha02.Institut;
import ha02.Person;
import ha02.Uni;
import ha02.Visitor;
/**
 * @author Xavier Ngansop
 * HA2: 06/10
 * -3 Visitor Pattern falsch
 * -1 Composite Pattern nicht teilweise richtig
 */
public class BudgetTest {

	@Test
	public void test() {
		
		Visitor visitor = new Visitor();
		
		Uni uni = new Uni("UniKassel",0);
		
		Fakultaet fakultaet = new Fakultaet("Fakultaet",0);
		
		Fachbereich fb15 = new Fachbereich("Maschinenbau",0);
		Fachbereich fb16 = new Fachbereich("Informatik",0);
		
		Institut fb16In1 = new Institut("Institut 1" , 25);
		Institut fb16In2 = new Institut("Institut 2" , 25);
		
		Fachgebiet fb15Fg1 = new Fachgebiet("fb15Fachgebiet1",10);
		Fachgebiet fb15Fg2 = new Fachgebiet("fb15Fachgebiet2",15);
		Fachgebiet fb15Fg3 = new Fachgebiet("fb15Fachgebiet3",25);
		
		Fachgebiet fb16Fg1 = new Fachgebiet("fb16Fachgebiet1",0);
		Fachgebiet fb16Fg2 = new Fachgebiet("fb16Fachgebiet2",25);
		Fachgebiet fb16Fg3 = new Fachgebiet("fb16Fachgebiet3",25);
		
		Fachgebiet umwelt = new Fachgebiet("Umwelttechnik",100);
		
		Person p1 = new Person("a","Professor",0);
		Person p2 = new Person("a","Doktorand",0);
		Person p3 = new Person("b","Professor",0);
		Person p4 = new Person("b","Doktorand",0);
		Person p5 = new Person("c","Professor",0);
		Person p6 = new Person("c","Doktorand",0);
		Person p7 = new Person("d","Professor",0);
		Person p8 = new Person("d","Doktorand",0);
		Person p9 = new Person("e","Professor",0);
		Person p10 = new Person("e","Doktorand",0);
		Person p11= new Person("f","Professor",0);
		Person p12= new Person("f","Doktorand",0);

		
		uni.addChild(fakultaet);
		uni.addChild(umwelt);                    // add fachgebiet directly under university
		
		fakultaet.addChild(fb15);					// merge both fachbereiche to one fakultaet
		fakultaet.addChild(fb16);
		
		fb15.addChild(fb15Fg1);						// add 3 fachgebiete to Fachbereich 15
		fb15.addChild(fb15Fg2);
		fb15.addChild(fb15Fg3);
		
		fb16.addChild(fb16In1);						// add 2 Institute to fachbereich 16
		fb16.addChild(fb16In2);
		
		fb16In1.addChild(fb16Fg1);					// add 2 Fachgebiete to Institute 1 
		fb16In1.addChild(fb16Fg2);
		fb16In2.addChild(fb16Fg3);					// add 1 Fachgebiete to Institute 2
		
		fb15Fg1.addChild(p1);						// add Persons to every Fachgebiet
		fb15Fg1.addChild(p2);
		
		fb15Fg2.addChild(p3);
		fb15Fg2.addChild(p4);
		
		fb15Fg3.addChild(p5);
		fb15Fg3.addChild(p6);
		
		fb16Fg1.addChild(p7);
		fb16Fg1.addChild(p8);

		fb16Fg2.addChild(p9);
		fb16Fg2.addChild(p10);
		
		fb16Fg3.addChild(p11);
		fb16Fg3.addChild(p12);
		
		
		
		assertTrue("fakulteat sollte 2 Fachbereiche haben", fakultaet.getChildren().size() == 2);    // fakultaet should have 2  children
		
		assertTrue("Der erste Fachbereich der fakultaet sollte Maschinenbau sein", fakultaet.getChildren().get(0).getName().equals("Maschinenbau"));
		
		assertTrue(" Fachbereich 15 sollte 3 Fachgebiete haben", fb15.getChildren().size() == 3);  			//  Fachbereich15 should have 3 children fachgebiete
		assertTrue(" Fachbereich 16 sollte  2 Institue haben", fb16.getChildren().size() == 2); 			// Fachbereuch 16 should have 2 children institute
		
		assertTrue("  Fachgebite in FB15 sollte 2 angestellte haben", fb15Fg1.getChildren().size() == 2 && fb15Fg2.getChildren().size() == 2 && fb15Fg3.getChildren().size() == 2);    // every Fachber
		assertTrue("  Fachgebite in FB16 sollte 2 angestellte haben", fb16Fg1.getChildren().size() == 2 && fb16Fg2.getChildren().size() == 2 && fb16Fg3.getChildren().size() == 2);
		
		
		visitor.visit(uni);			// set Visitor on Rootnode and adds Fachgebiet budgets in sumBudget
		
		assertTrue("die Fachgebiete sollten zusammen 200 Budget haben", fb15Fg1.getBudget()+fb15Fg2.getBudget()+fb15Fg3.getBudget()+fb16Fg1.getBudget()+fb16Fg2.getBudget()+fb16Fg3.getBudget()+umwelt.getBudget() == visitor.getSumBudget());
		
		System.out.println("GesamtBudget ist "+visitor.getSumBudget());
		
		
		
	}
	
}
