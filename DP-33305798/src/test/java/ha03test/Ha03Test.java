package ha03test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ha03.Interpreter;

/**
 * HA3: 10/10
 */
public class Ha03Test {

	@Test
	public void test() {
		
		Interpreter inter = new Interpreter();
		
		String text = "Ldc6\nLdc7\nMult\nStore x\nLd 5\nPrint";
		String[] parts = text.split("\n");
		
		
		inter.addDefaultHandler();    // adds every Handler once
		
		inter.interpretText(parts);
		
		
		
	/** Erste Version der Ha, da wurden die Strings noch einzeln eingelesen. Nur zur ablauf uebersicht beibehalten
	 * 
	 * 	inter.interpret("Ldc6");
		assertTrue("first in stack should be 6", (Integer)inter.getStack().peek() == 6 );    
		
		inter.interpret("Ldc7");
		assertTrue("ontop of stack should be 7", (Integer)inter.getStack().peek() == 7 );  
		
		inter.interpret("Mult");
		
		inter.interpret("Store x");
		assertTrue("Stack should be empty", inter.getStack().isEmpty());  
		
		inter.interpret("Ld 5");
		assertTrue("ontop of stack should be 5", (Integer)inter.getStack().peek() == 5 );  
		
		inter.interpret("Print");
	 */
		
		assertTrue("Stack should be empty", inter.getStack().isEmpty());  
		
		
		
		
		
	}
}
