package ha06Test;




import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import ha06.Main;

import javafx.stage.Stage;
import junit.framework.Assert;


// Wieso diese externe libriaries?? Use Gradle
// HA6: 09/10
// -1 Externe Libriaries +  Test kann ich nicht starten
public class Ha06Test extends ApplicationTest {

	private Main main;
	
	@Override
	public void start(Stage stage) {
		
		main = new Main();
		main.start(stage);
	}
	
	@Test
	public void testHa04() throws IOException, InterruptedException {
		
		
		main.getTextfield().setText("line l1 100 0 0 100");
		clickOn(main.getButton());
		
		main.getTextfield().setText("line l2 100 0 200 100");
		clickOn(main.getButton());

		
		main.getTextfield().setText("line l3 0 100 200 100");
		clickOn(main.getButton());
		
		main.getTextfield().setText("line l4 0 100 0 300");
		clickOn(main.getButton());
		

		main.getTextfield().setText("line l5 200 100 200 300");
		clickOn(main.getButton());
		

		main.getTextfield().setText("line l6 0 300 200 300");
		clickOn(main.getButton());
		
		main.getTextfield().setText("line l7 0 100 200 300");
		clickOn(main.getButton());
		
		main.getTextfield().setText("line l8 0 300 200 100");
		clickOn(main.getButton());
		
		main.getTextfield().setText("group g1 l1 l2 l3 l4 l5 l6 l7 l8");
		clickOn(main.getButton());
		
		main.getTextfield().setText("clone g1 g2 350 0");
		clickOn(main.getButton());
		
		main.getTextfield().setText("clone g2 g3 350 0");
		clickOn(main.getButton());
		

		assertTrue(" es sollten 3 Gruppen existieren", main.getGroup().getGruppen().size() == 3);  
		
//		while(true) {
//			
//		}
	}


	
}
