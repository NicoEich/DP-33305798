package ha07;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

//import org.assertj.core.internal.bytebuddy.agent.builder.AgentBuilder.Default.Transformation.Simple;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class GitLabCrawler implements MqttCallback {

	public static final String COMMIT_TIME = "commitTime";
	public static final String CRAWL_TIME = "crawlTime";

	private MqttClient mqttClient = null;
	private String mqttTopic = "uniks.gitGrabberServer";
	private String mqttSubscribeTopic = "uniks.gitCrawler";

	private String gitUrl;
	private String mqttUrl;
	private ScheduledExecutorService crawlExecutor;

	public static void main(String[] args) {
		new GitLabCrawler().start(args);
	}

	public void start(String[] args) {

		try {
			gitUrl = args[0];

			if (args.length > 1) {

				mqttUrl = args[1];
			

			mqttClient = new MqttClient(mqttUrl, gitUrl);   // giturl as id
			mqttClient.setCallback(this);
			mqttClient.connect();
			mqttClient.subscribe(mqttSubscribeTopic);
			
			}

		} catch (MqttException e) {
			e.printStackTrace();
		}

		crawlExecutor = Executors.newSingleThreadScheduledExecutor();

		crawlExecutor.execute(new Runnable() {

			@Override
			public void run() {
				doCrawl("repeated");

			}
		});
	}

	private void doCrawl(String repeatCmd) {

		try {

			URL url = new URL(gitUrl);

			URLConnection connection = url.openConnection();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String inputLine;
			String state = "searching";

			JsonObject commitObj = new JsonObject();
			commitObj.addProperty("gitUrl", gitUrl); // 618

			while ((inputLine = in.readLine()) != null) {

				System.out.println(state);

				if (state.equals("searching") && inputLine.indexOf(
						"<div class=\"info-well d-none d-sm-block project-last-commit append-bottom-default\">") >= 0) { // set
																															// search
																															// state
																															// if
																															// search
																															// are
																															// if
					// found and line is = and not line 0
					state = "search message";
				}

				if (state.equals("search message") && inputLine.indexOf("<a class=\"commit-row-message") >= 0) { // if
																													// search
																													// area
																													// is
																													// found
																													// search

					int startPos = inputLine.indexOf(">"); // start of message
					int endPos = inputLine.indexOf("<", startPos); // end of message

					String commitMessage = inputLine.substring(startPos + 1, endPos); // TODO: set start position +2 for
																						// ">
					commitObj.addProperty("message", commitMessage);

					state = "search author"; // if message is found look for author
				}

				if (state.equals("search author")
						&& inputLine.indexOf("<a class=\"commit-author-link has-tooltip\"") >= 0) { // if search area is
																									// found search
					// for X

					int startPos = inputLine.indexOf(">"); // start of message
					int endPos = inputLine.indexOf("</a>", startPos); // end of message

					String commitAuthor = inputLine.substring(startPos + 1, endPos); // TODO: set start position
					commitObj.addProperty("author", commitAuthor);

					state = "search time"; // if message is found look for author

					startPos = inputLine.indexOf("datetime=\""); // start of message
					endPos = inputLine.indexOf("\" ", startPos); // end of message

					String commitTime = inputLine.substring(startPos + 10, endPos - 1); // TODO: set start position

					commitObj.addProperty(COMMIT_TIME, commitTime);

					state = "search end"; // if message is found look for author
				}

				// if (state.equals("search time") && inputLine.indexOf("<a
				// class=\\\"commit-author-link has-tooltip\\\"") >= 0) { // if search area is
				// found search
				// // for X
				//
				// int startPos = inputLine.indexOf("datetime=\""); // start of message
				// int endPos = inputLine.indexOf("\" ", startPos); // end of message
				//
				// String commitTime = inputLine.substring(startPos+10, endPos-1); // TODO: set
				// start position
				//
				// commitObj.addProperty(COMMIT_TIME, commitTime);
				//
				// System.out.println("time found");
				// state = "search end"; // if message is found look for author
				// }

				if (state.equals("search end") && inputLine.indexOf("") >= 0) {
					state = "end of list item";
					break;
				}

			}
			in.close();

			DateFormat dateTimeInstance = SimpleDateFormat.getDateTimeInstance(); // dateformat
			Date now = new Date(System.currentTimeMillis()); // currente time

			String crawlTimeString = dateTimeInstance.format(now); // make String in dateformat with current time (now)
			commitObj.addProperty(CRAWL_TIME, crawlTimeString);

			// now upload to mqtt
			// server----------------------------------------------------------------------------------------

			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String json = gson.toJson(commitObj) + "\n";

			MqttMessage message = new MqttMessage(json.getBytes());
			mqttClient.publish(mqttTopic, message);
			

			System.out.println("message:  " + json);

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttPersistenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
		finally {
			if (repeatCmd != null && repeatCmd.equals("repeated")) {

				crawlExecutor.schedule(new Runnable() {

					@Override
					public void run() {
						doCrawl("repeated");

					}
				}, 1, TimeUnit.MINUTES); // wait x Timeunits

			}
		}
	}

	@Override
	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub

	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		// TODO Auto-generated method stub

		
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub

		
	}

}
