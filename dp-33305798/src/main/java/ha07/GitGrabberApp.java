package ha07;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.internal.wire.MqttConnect;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.LinkedHashMap;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GitGrabberApp extends Application implements MqttCallback {

	private MqttClient mqttClient;
	private LinkedHashMap<String, TextArea> textAreaMap = new LinkedHashMap<>();
	private VBox commitBox;
	private String mqttUrl = "tcp://avocado.uniks.de:41883";
	private String mqttSubscribeTopic = "uniks.gitGrabberServer";

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {

		mqttConnect();
		commitBox = new VBox();
		commitBox.setSpacing(10);
	

		Scene scene = new Scene(commitBox, 500, 700);
	
		stage.setTitle("GitGrabberApp");
		stage.setScene(scene);
		stage.show();
		
		commitBox.getChildren().add(new TextArea("\"gitUrl\": \"https://gitlab.com/gitlab-org/gitlab-ce\",\r\n" + 
				"  \"message\": \"Merge branch \\u0027update-mr-img-docs\\u0027 into \\u0027master\\u0027\\n\\nUpdate merge_request.png in docs\\n\\nSee merge request gitlab-org/gitlab-ce!20730\",\r\n" + 
				"  \"author\": \"Mike Lewis\",\r\n" + 
				"  \"commitTime\": \"2018-07-22T05:29:42.000Z\",\r\n" + 
				"  \"crawlTime\": \"2018-07-22T13:27:03.163Z[Etc/UTC]\",\r\n" + 
				"  \"greetings\": \"Hallo :)\"")); 

		// MqttMessage mqttMessage = new MqttMessage("resend".getBytes());
		// mqttClient.publish("uniks.gitCrawler", mqttMessage);

	}

	public void mqttConnect() throws MqttException {

		mqttClient = new MqttClient(mqttUrl, "a" + System.currentTimeMillis()); // server adress and client id (current
																				// time)
		mqttClient.setCallback(this);
		mqttClient.connect();
		mqttClient.subscribe(mqttSubscribeTopic);
	}

	private void handleMqttMessage(String topic, MqttMessage message) {

		byte[] payload = message.getPayload();
		String msg = new String(payload);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonObject jsonObject = gson.fromJson(msg, JsonObject.class);

		String gitUrl = jsonObject.get("gitUrl").getAsString();

		System.out.println("test");
		commitBox.getChildren().add(new TextArea("test")); 
		
		
		TextArea textArea = textAreaMap.get(gitUrl);

		if (textArea == null) {
			textArea = new TextArea(msg);
			textAreaMap.put(gitUrl, textArea);
			commitBox.getChildren().add(textArea);
		}
		textArea.setText(msg);
	
		
	}

	@Override
	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub

		boolean done = false;

		while (!done) {

			try {

				Thread.sleep(100 * 1000);

				System.out.println("Try reconnect");

				mqttClient.connect();

				mqttClient.subscribe(mqttSubscribeTopic);

				done = true;

				System.out.println("reconnected");

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // sleep 100 secs
			catch (MqttSecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {

		System.out.println("nachricht in app angekommen");

		byte[] payload = message.getPayload();
		String msg = new String(payload);
		System.out.println(msg);

		handleMqttMessage(topic, message);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub

	}

}
